import sys
import time
import logging
from random import random
from typing import Optional

import requests
from bs4 import BeautifulSoup
import simpleaudio as sa


product_map = {
    "Radeon_6700XT": 5496921400,
    "Radeon_6800": 5458374000,
    "Radeon_6800XT": 5458374100,
    "Radeon_6800XT_MB": 5496921500,
    "Radeon_6900XT": 5458374200,

    "Ryzen_5600X": 5450881700,
    "Ryzen_5800X": 5450881600,
    "Ryzen_5900X": 5450881500,
    "Ryzen_5950X": 5450881400,
}


def setup_logger() -> logging.Logger:
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    file_handler = logging.FileHandler("alert.log")
    file_handler.setLevel(logging.INFO)
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s %(message)s')
    file_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    return logger


def get_error_no(product_id: int) -> Optional[int]:
    url = f"http://store.digitalriver.com/store?Action=buy&Env=BASE&Locale=de_DE&SiteID=defaults&ProductID={product_id}"
    res = requests.get(url)
    soup = BeautifulSoup(res.text, "lxml")

    try:
        error_paragraph = soup.select_one(".dr_error")
        error_num_para = error_paragraph.select("p")[-1]
        error_no = int(error_num_para.text.split("CAT_")[-1])
    except:
        return None

    return error_no


if __name__ == "__main__":

    IN_STOCK_ERROR_NO = 16
    NOT_IN_STOCK_ERROR_NO = 15

    SLEEP_TIME = 30
    SLEEP_TIME_DEVIATION = 5  # + and -

    # edit here
    prods_of_interest = ["Radeon_6800XT", "Radeon_6800XT_MB", "Radeon_6900XT"]

    logger = setup_logger()

    alarm_wave_obj = sa.WaveObject.from_wave_file("alarm.wav")
    alarm_play_obj = None

    states = {poi: None for poi in prods_of_interest}

    logger.info("starting a new logging session now")

    while True:
        for p in prods_of_interest:
            p_id = product_map[p]
            error_no = get_error_no(p_id)
            last_error_no = states[p]

            if error_no == IN_STOCK_ERROR_NO:
                if alarm_play_obj is None or not alarm_play_obj.is_playing():
                    alarm_play_obj = alarm_wave_obj.play()

            if error_no != last_error_no:
                if error_no == NOT_IN_STOCK_ERROR_NO:
                    logger.info(f"{p} not in stock")
                elif error_no == IN_STOCK_ERROR_NO:
                    logger.info(f"{p} new in stock")
                else:
                    logger.info(f"something else happened to {p}")

                states[p] = error_no

                time.sleep((random() * 2))

        deviation = random() * SLEEP_TIME_DEVIATION * 2 - SLEEP_TIME_DEVIATION
        logger.info(f"sleeping for {(SLEEP_TIME + deviation):.2f} seconds now")
        time.sleep(SLEEP_TIME + deviation)

